use crate::storage::{Edge, GridOffset, Node};
use std::{
  collections::HashMap,
  fs::File,
  io::{BufRead, BufReader},
  sync::mpsc::{channel, Receiver},
};
use threadpool::ThreadPool;

pub struct RoutingData {
  nodes: HashMap<i64, Node>,
  edges: HashMap<i64, Edge>,
  grid_offsets: HashMap<String, GridOffset>,
  node_count: i32,
  edge_count: i32,
}

impl RoutingData {
  pub fn new() -> RoutingData {
    let mut data = RoutingData {
      nodes: HashMap::new(),
      edges: HashMap::new(),
      grid_offsets: HashMap::new(),
      node_count: 643169,
      edge_count: 1566688,
    };
    data.load();
    data
  }

  pub fn load(&mut self) {
    let pool = ThreadPool::new(2);
    let rx_grid_offsets = self.load_grid_offsets(&pool);
    let rx_nodes = self.load_nodes(&pool);
    let rx_edges = self.load_edges(&pool);
    self.grid_offsets = rx_grid_offsets.recv().expect("load grid offsets falied");
    self.nodes = rx_nodes.recv().expect("load nodes failed");
    self.edges = rx_edges.recv().expect("load edges failed");
    eprintln!("self.nodes.len() = {:#?}", self.nodes.len());
    eprintln!("self.edges.len() = {:#?}", self.edges.len());
    eprintln!("self.grid_offsets.len() = {:#?}", self.grid_offsets.len());
  }

  pub fn load_grid_offsets(&self, pool: &ThreadPool) -> Receiver<HashMap<String, GridOffset>> {
    let (tx, rx) = channel();
    let mut grid_offsets: HashMap<String, GridOffset> = HashMap::new();
    pool.execute(move || {
      let file = File::open("files/raw_routingdata/grid.data").expect("error reading file grid.data");
      let mut key = "".to_string();
      let mut i = 0;
      for line in BufReader::new(file).lines() {
        let line_data = line.unwrap();
        if i % 2 == 0 {
          key = line_data;
        } else {
          let grid_offset_data: Vec<&str> = line_data.split(";").collect();
          grid_offsets.insert(
            String::from((key.split(":").collect::<Vec<&str>>())[1]),
            GridOffset::new(grid_offset_data[0], grid_offset_data[1].parse().unwrap()),
          );
        }
        i += 1;
      }
      tx.send(grid_offsets).expect("sending grid offsets failed");
    });
    rx
  }

  pub fn load_edges(&self, pool: &ThreadPool) -> Receiver<HashMap<i64, Edge>> {
    let (tx, rx) = channel();
    let mut edges: HashMap<i64, Edge> = HashMap::new();
    pool.execute(move || {
      let file = File::open("files/raw_routingdata/edges.data").expect("error reading file edges.data");
      let mut key = "".to_string();
      let mut i = 0;
      let mut job_count = 0;
      let (tx1, rx1) = channel();
      let pool1 = ThreadPool::new(3);
      for line in BufReader::new(file).lines() {
        let line_data = line.unwrap();
        if i % 2 == 0 {
          key = line_data;
        } else {
          job_count += 1;
          let tx1 = tx1.clone();
          let line_data = line_data.clone();
          let key = key.clone();
          pool1.execute(move || {
            let edge_data: Vec<&str> = line_data.split(";").collect();
            let edge_access_data: Vec<&str> = edge_data[7].split("|").collect();
            let mut edge_access: Vec<bool> = Vec::new();
            for access_datum in &edge_access_data {
              edge_access.push(access_datum.parse().unwrap())
            }
            let mut edge = Edge::new(
              edge_data[0],
              edge_data[1].parse().unwrap(),
              edge_data[2].parse().unwrap(),
            );
            edge.next_crossing = edge_data[3].parse().unwrap();
            edge.distance = edge_data[4].parse().unwrap();
            edge.speed = edge_data[5].parse().unwrap();
            edge.additional_weight = edge_data[6].parse().unwrap();
            edge.access = edge_access;
            tx1
              .send(((key.split(":").collect::<Vec<&str>>())[1].parse::<i64>().unwrap(), edge))
              .unwrap();
          });
        }
        i += 1;
      }
      for _ in 0..job_count {
        let edge_data = rx1.recv().unwrap();
        edges.insert(edge_data.0, edge_data.1);
      }
      tx.send(edges).expect("sending edges failed");
    });
    rx
  }

  pub fn load_nodes(&self, pool: &ThreadPool) -> Receiver<HashMap<i64, Node>> {
    let (tx, rx) = channel();
    let mut nodes: HashMap<i64, Node> = HashMap::new();
    pool.execute(move || {
      let file = File::open("files/raw_routingdata/nodes.data").expect("error reading file nodes.data");
      let mut key = "".to_string();
      let mut i = 0;
      let mut job_count = 0;
      let (tx1, rx1) = channel();
      let pool1 = ThreadPool::new(2);
      for line in BufReader::new(file).lines() {
        let line_data = line.unwrap();
        if i % 2 == 0 {
          key = line_data;
        } else {
          job_count += 1;
          let tx1 = tx1.clone();
          let line_data = line_data.clone();
          let key = key.clone();
          pool1.execute(move || {
            let node_data: Vec<&str> = line_data.split(";").collect();
            let mut node = Node::new(node_data[1].parse().unwrap(), node_data[2].parse().unwrap());
            node.id = node_data[0].parse().unwrap();
            node.offset_pointer = node_data[3].parse().unwrap();
            node.crossing = node_data[4].parse().unwrap();
            tx1
              .send(((key.split(":").collect::<Vec<&str>>())[1].parse::<i64>().unwrap(), node))
              .unwrap();
          });
        }
        i += 1;
      }
      for _ in 0..job_count {
        let node_data = rx1.recv().unwrap();
        nodes.insert(node_data.0, node_data.1);
      }
      tx.send(nodes).expect("sending nodes failed");
    });
    rx
  }
}
