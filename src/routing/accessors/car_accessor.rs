use crate::{routing::accessors::WayAccessor, storage::Edge};

pub struct CarAccessor {}

impl WayAccessor for CarAccessor {
  fn new() -> CarAccessor {
    CarAccessor {}
  }

  fn can_access_way(&self, edge: &Edge) -> bool {
    edge.is_car_allowed()
  }

  fn get_max_speed(&self) -> i32 {
    130
  }
}
