use crate::storage::Edge;

pub trait WayAccessor {
  fn new() -> Self;
  fn can_access_way(&self, edge: &Edge) -> bool;
  fn get_max_speed(&self) -> i32;
}
