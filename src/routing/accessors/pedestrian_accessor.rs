use crate::{routing::accessors::WayAccessor, storage::Edge};

pub struct PedestrianAccessor {}

impl WayAccessor for PedestrianAccessor {
  fn new() -> Self {
    PedestrianAccessor {}
  }

  fn can_access_way(&self, edge: &Edge) -> bool {
    edge.is_pedestrian_allowed()
  }

  fn get_max_speed(&self) -> i32 {
    7
  }
}
