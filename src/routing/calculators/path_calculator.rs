use crate::{
  routing::{accessors::WayAccessor, calculators::CalculationResult},
  storage::{Edge, Node},
};
use std::collections::HashMap;

pub trait PathCalculator<'a, T>
where T: WayAccessor
{
  fn new(distances: HashMap<i64, f64>, way_accessor: &'a T) -> Self;
  fn check_neighbour_and_costs(
    &self,
    node: &Node,
    edge: &Edge,
    crossing_node: Option<&Node>,
  ) -> Result<CalculationResult, ()>;
  fn get_existing_weight(&self, node_id: i64) -> f64;
  fn calculate_costs_to_neighbor(
    &self,
    node: &Node,
    edge: &Edge,
    crossing_node: Option<&Node>,
  ) -> Result<CalculationResult, ()>;
  fn get_speed_in_meter_per_seconds(&self, edge: &Edge) -> f64;
}
