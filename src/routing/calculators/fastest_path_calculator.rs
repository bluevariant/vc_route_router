use crate::{
  routing::{
    accessors::WayAccessor,
    calculators::{CalculationResult, PathCalculator},
  },
  storage::{Edge, Node},
  util::GraphUtil,
};
use std::collections::HashMap;

pub struct FastestPathCalculator<'a, T>
where T: WayAccessor
{
  pub distances: HashMap<i64, f64>,
  pub way_accessor: &'a T,
}

impl<'a, T> PathCalculator<'a, T> for FastestPathCalculator<'a, T>
where T: WayAccessor
{
  fn new(distances: HashMap<i64, f64>, way_accessor: &'a T) -> Self {
    FastestPathCalculator {
      distances,
      way_accessor,
    }
  }

  fn check_neighbour_and_costs(
    &self,
    node: &Node,
    edge: &Edge,
    crossing_node: Option<&Node>,
  ) -> Result<CalculationResult, ()>
  {
    if self.get_speed_in_meter_per_seconds(edge) > 0f64 {
      let target_node_id = match crossing_node {
        Some(v) => v.id,
        None => edge.target_node,
      };
      let distance = match crossing_node {
        Some(v) => GraphUtil::get_distance(&node, &v),
        None => edge.distance,
      };
      let distance_time = distance / self.get_speed_in_meter_per_seconds(edge);
      let mut weight_to_neighbour = self.get_existing_weight(node.id) + distance_time;
      weight_to_neighbour += 10f64 * weight_to_neighbour * edge.additional_weight;
      if self.get_existing_weight(target_node_id) > weight_to_neighbour {
        return Ok(CalculationResult::new(weight_to_neighbour, distance, distance_time))
      }
    }
    Err(())
  }

  fn get_existing_weight(&self, node_id: i64) -> f64 {
    if self.distances.contains_key(&node_id) {
      return self.distances[&node_id]
    }
    std::f64::MAX
  }

  fn calculate_costs_to_neighbor(
    &self,
    node: &Node,
    edge: &Edge,
    crossing_node: Option<&Node>,
  ) -> Result<CalculationResult, ()>
  {
    if self.way_accessor.can_access_way(&edge) && edge.additional_weight < 1.0 {
      match self.check_neighbour_and_costs(node, edge, crossing_node) {
        Ok(v) => return Ok(v),
        Err(e) => return Err(e),
      }
    }
    Err(())
  }

  fn get_speed_in_meter_per_seconds(&self, edge: &Edge) -> f64 {
    let mut speed = edge.speed;
    if speed > self.way_accessor.get_max_speed() {
      speed = self.way_accessor.get_max_speed();
    }
    speed as f64 / 3.6
  }
}
