pub struct CalculationResult {
  pub weight: f64,
  pub distance: f64,
  pub distance_time: f64,
}

impl CalculationResult {
  pub fn new(weight: f64, distance: f64, distance_time: f64) -> CalculationResult {
    CalculationResult {
      weight,
      distance,
      distance_time,
    }
  }
}
