#[derive(Clone, Debug)]
pub struct GridOffset {
  pub name: String,
  pub offset: i64,
  pub next_offset: String,
}

impl GridOffset {
  pub fn new(name: &str, offset: i64) -> GridOffset {
    GridOffset {
      name: name.to_string(),
      offset,
      next_offset: "".to_string(),
    }
  }
}
