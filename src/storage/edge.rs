#[derive(Clone, Debug)]
pub struct Edge {
  pub edge_type: String,
  pub source_node: i64,
  pub target_node: i64,
  pub next_crossing: i32,
  pub distance: f64,
  pub additional_weight: f64,
  pub speed: i32,
  pub access: Vec<bool>,
}

impl Edge {
  pub fn new(edge_type: &str, source_node: i64, target_node: i64) -> Edge {
    Edge {
      edge_type: edge_type.to_string(),
      source_node,
      target_node,
      next_crossing: -1,
      distance: 0.0,
      additional_weight: 0.0,
      speed: 0,
      access: vec![false, false],
    }
  }

  pub fn is_car_allowed(&self) -> bool {
    self.access[0]
  }

  pub fn is_pedestrian_allowed(&self) -> bool {
    self.access[1]
  }
}
