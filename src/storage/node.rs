#[derive(Clone, Debug)]
pub struct Node {
  pub id: i64,
  pub lat: f64,
  pub lon: f64,
  pub offset_pointer: i64,
  pub crossing: bool,
}

impl Node {
  pub fn new(lat: f64, lon: f64) -> Node {
    Node {
      id: 0,
      lat,
      lon,
      offset_pointer: 0,
      crossing: false,
    }
  }
}
