#![allow(dead_code)]

mod routing;
mod storage;
mod util;

fn main() {
  routing::RoutingData::new();
}
