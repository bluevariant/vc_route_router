use crate::storage::Node;

pub struct GraphUtil {}

impl GraphUtil {
  pub fn get_distance(node: &Node, target: &Node) -> f64 {
    let r = 6372800f64;
    let mut lat1 = node.lat;
    let mut lat2 = target.lat;
    let d_lat = (lat2 - lat1).to_radians();
    let d_lon = (target.lon - node.lon).to_radians();
    lat1 = lat1.to_radians();
    lat2 = lat2.to_radians();
    let a = f64::sin(d_lat / 2f64).powi(2) + f64::sin(d_lon / 2f64).powi(2) * lat1.cos() * lat2.cos();
    let c = 2f64 * a.sqrt().asin();
    r * c
  }
}
